﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialVelocity : MonoBehaviour
{
    [SerializeField] Vector3 initialVelocity;
    [SerializeField] Vector3 initialW;
   private Rigidbody rigidBody;

    // Use this for initialization
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.velocity = initialVelocity;
        rigidBody.angularVelocity = initialW;
    }
}
