# Unstable Rotation

## Commits

* Initial commit - repo created
* Simulate iPhone 8 Puls spinning
* Calculate inertia Tensors
* Adding torque to a sphere
* Adding Magnus Effect
    * Need to revisit this section to update for newer versions of Unity